const nameList = [
  'AVENGERS',
  'SPIDER-MAN',
  'IRON MAN',
  'BLACK PANTHER',
  'DEADPOOL',
  'CAPTAIN AMERICA',
  'JESSICA JONES',
  'ANT-MAN',
  'CAPTAIN MARVEL',
  'GUARDIANS OF THE GALAXY',
  'WOLVERINE',
  'LUKE CAGE',
];

export const getCharacters = (amount, search) =>  {
  const filteredNameList = nameList
    .filter((name) => !search || name.indexOf(search.toUpperCase()) === 0);

  return Array.from({ length: amount })
    .map((_, index) => ({
      id: index,
      name: filteredNameList[index],
      thumbnail: '',
    }));
}

export const getCharacter = (id) =>  ({
  id,
  name: 'Hero 1',
  thumbnail: 'http://image.mocked',
  description: 'Description',
  series:[
    { name: 'Serie 1' },
  ],
  comics:[
    {name: 'Comic 1'},
    {name: 'Comic 2'},
  ],
});

