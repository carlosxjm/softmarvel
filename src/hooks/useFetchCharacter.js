import { gql } from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';

export const CHARACTER_QUERY = gql`
  query characters($where: CharacterWhereInput, $offset: Int, $limit: Int) {
    characters(where: $where, offset: $offset, limit: $limit) {
      id
      name
      thumbnail
      description
      series {
        name
      }
      comics {
        name
      }
      editedDescription @client(always: true)
    }
  }
`;

const useFetchCharacter = ({ id  }) => {
  const { data, ...queryProps } = useQuery(CHARACTER_QUERY, {
    variables: {
      where: { id },
    },
    options: { fetchPolicy: 'no-cache' },
  });

  return {
    character: (data && data.characters[0]) || null,
    ...queryProps,
  };
};

export default useFetchCharacter;
