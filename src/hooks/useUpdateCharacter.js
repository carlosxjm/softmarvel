import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';

export const UPDATE_CHARACTER = gql`
  mutation updateLocalCharacter($character: CharacterState!) {
    updateLocalCharacter(character: $character) @client
  }
`;

const useUpdateCharacter = () => {
  const [mutation] = useMutation(UPDATE_CHARACTER);
  return (character) => mutation({ variables: { character: { ...character } } });
};

export default useUpdateCharacter;

