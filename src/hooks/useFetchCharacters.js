import { gql } from 'apollo-boost';
import { useQuery } from '@apollo/react-hooks';

export const CHARACTERS_QUERY = gql`
  query characters($where: CharacterWhereInput, $offset: Int, $limit: Int) {
    characters(where: $where, offset: $offset, limit: $limit) {
      id
      name
      thumbnail
    }
  }
`;

const useFetchCharacters = ({ searchTerm = '', page = 1, pageSize = 6 }) => {
  const where = searchTerm ? {nameStartsWith: searchTerm} : {};
  const { data, ...queryProps } = useQuery(CHARACTERS_QUERY, {
    variables: {
      offset: pageSize,
      limit: (page - 1) * pageSize,
      where,
    },
  });

  return {
    characters: (data && data.characters) || [],
    ...queryProps,
  };
};

export default useFetchCharacters;
