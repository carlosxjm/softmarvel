import React from 'react';
import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  from,
} from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';
import { withClientState } from 'apollo-link-state';
import { defaults, resolvers } from './localState';

const cache = new InMemoryCache({ addTypename: false });

const stateLink = withClientState({
  cache,
  defaults,
});

const client = new ApolloClient({
  link: from([
    stateLink,
    new HttpLink({ uri: process.env.REACT_APP_API_URL }),
  ]),
  cache,
  resolvers,
});

const ApolloClientProvider = ({ children }) => (
  <ApolloProvider client={client}>
    {children}
  </ApolloProvider>
);

export default ApolloClientProvider;
