import { gql } from 'apollo-boost';

export const GET_CHARACTER = gql`
  query localCharacter {
    localCharacters @client {
      id
      description
    }
  }
`;

export const updateLocalCharacter = (_, { character }, { cache }) => {
  const { localCharacters: previousCharacterList } = cache.readQuery({
    query: GET_CHARACTER,
  });

  const data = {
    localCharacters: [
      ...previousCharacterList.filter(({ id }) => id !== character.id),
      character,
    ],
  };

  cache.writeData({ query: GET_CHARACTER, data });
  return null;
};

export const editedDescription = ({ id }, _, { cache }) => {
  const { localCharacters } = cache.readQuery({
    query: GET_CHARACTER,
  });
  const localCharacter = localCharacters.find((character) => character.id === id);
  return localCharacter ? localCharacter.description : '';
};
