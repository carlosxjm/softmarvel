import { updateLocalCharacter, editedDescription } from './character';

export const defaults = {
  localCharacters: [],
  editedDescription: '',
};

export const resolvers = {
  Mutation: { updateLocalCharacter },
  Query: { editedDescription },
};
