import App from './App';

describe('App component', () => {
  it('renders application title', async () => {
    const { container } = global.renderComponentToTest(App);
    global.runAsyncTimers();

    expect(container).toBeInTheDocument();
  });
});

