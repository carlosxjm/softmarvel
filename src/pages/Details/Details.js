import React from 'react';
import { useParams } from "react-router-dom";
import useFetchCharacter from '../../hooks/useFetchCharacter';
import EditForm from '../../components/EditForm/EditForm';
import DetailStyle from './Details.style';
import Loader from '../../components/Loader/Loader';

const Details = () => {
  let { id } = useParams();
  const { character, loading, refetch } = useFetchCharacter({ id: +id });
  return loading ? <Loader /> : (
    <DetailStyle>
      <img className="image" src={character.thumbnail} alt={`${character.name} poster`} />
      <h1 className="name">{character.name}</h1>
      <div className="about">
        <EditForm characterId={character.id} onSubmit={refetch} />
        {(character.editedDescription || character.description) &&
          <>
            <h2>Sobre</h2>
            <p>{character.editedDescription || character.description}</p>
          </>
        }
      </div>

      <div className="series">
        <h3>Series</h3>
        <ul>
          {
            character.series.map((serie, index) => (
              <li key={+index}>{serie.name}</li>
            ))
          }
        </ul>
      </div>
      <div className="comics">
        <h3>Comics</h3>
        <ul>
          {
            character.comics.map((comic, index) => (
              <li key={+index}>{comic.name}</li>
            ))
          }
        </ul>
      </div>
    </DetailStyle>
  )
}


export default Details;
