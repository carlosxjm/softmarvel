import { getCharacter } from '../../../mocks/characters';
import { CHARACTER_QUERY } from '../../hooks/useFetchCharacter';
import Details from './Details';

const getMock = () => ({
  request: {
    query: CHARACTER_QUERY,
    variables: {
      where: { id: 1 },
    },
  },
  result: {
    data: {
      characters: [getCharacter(1)],
    },
  },
});

describe('Details component', () => {
  it('renders page properly', () => {
    const mocks = [getMock()];
    const { container } = global.renderComponentToTest(Details, { mocks });
    expect(container).toBeInTheDocument();
  });

  it('renders character name', async (done) => {
    const mocks = [getMock()];
    const { getByText } = global.renderComponentToTest(Details, { mocks });
    await global.runAsyncTimers();
    process.nextTick(() => {
      global.runAsyncTimers();
      const name = getByText(mocks[0].result.data.characters[0].name);
      expect(name).toBeInTheDocument();
      done();
    });
  });
});

