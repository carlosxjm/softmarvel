import styled from 'styled-components';

const DetailStyle = styled.div.attrs({ className: 'DetailStyle' })`
  padding: 30px 0;
  text-align: center;
  h1, h2, h3 {
    margin-bottom: 10px;
  }
  .image {
    width: 200px;
    height: 200px;
    margin-bottom: 30px;
    border-radius: 50%;
    overflow: hidden;
  }
  .about, .series, .comics {
    text-align: left;
    margin-bottom: 30px;
    position: relative;
    ul {
      padding: 0 0 0 20px;
    }
  }
  .edit-button {
    position: absolute;
    right: 0;
    top: 0;
  }
`;

export default DetailStyle;
