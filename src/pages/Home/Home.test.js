import { fireEvent } from '@testing-library/react';
import { getCharacters } from '../../../mocks/characters';
import { CHARACTERS_QUERY } from '../../hooks/useFetchCharacters';
import Home from './Home';


const getMock = (amount, search) => ({
  request: {
    query: CHARACTERS_QUERY,
    variables: {
      offset: 6,
      limit: 0,
      where: search ? { nameStartsWith: search } : {},
    },
  },
  result: {
    data: {
      characters: getCharacters(amount, search),
    },
  },
});

describe('Home component', () => {
  it('renders page title', () => {
    const { getByText } = global.renderComponentToTest(Home);
    global.runAsyncTimers();

    const title = getByText('SoftMarvel');
    expect(title).toBeInTheDocument();
  });

  it('renders characters', (done) => {
    const mocks = [getMock(1)];
    const { getByText } = global.renderComponentToTest(Home, { mocks });
    global.runAsyncTimers();
    process.nextTick(() => {
      const name = getByText(mocks[0].result.data.characters[0].name);
      expect(name).toBeInTheDocument();
      done();
    });
  });

  it('refetch query on search', (done) => {
    const search = 'captain';
    const mocks = [
      getMock(6),
      getMock(6),
    ];

    const { getByText, getByPlaceholderText, getByTestId } = global.renderComponentToTest(Home, { mocks });
    global.runAsyncTimers();

    const button = getByText('Buscar');
    const input = getByPlaceholderText('Busque um personagem');

    fireEvent.change(input, { target: { value: search } });
    fireEvent.click(button, { key: 'Enter', code: 'Enter' });
    const loader = getByTestId('loader');
    expect(loader).toBeInTheDocument();
    done();
  });

});

