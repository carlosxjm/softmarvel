import React, { useState } from 'react';
import useFetchCharacters from '../../hooks/useFetchCharacters';
import SearchForm from '../../components/SearchForm/SearchForm';
import Loader from '../../components/Loader/Loader';
import NoContent from '../../components/NoContent/NoContent';
import HomeStyle, { CharacterCardListStyle, CharacterCardStyle } from './Home.style';

const Home = () => {
  const [searchTerm, setSearchTerm] = useState();
  const { characters, loading, refetch } = useFetchCharacters({ page: 1, searchTerm });
  const handleSubmit = (value) => {
    setSearchTerm(value);
    refetch();
  }

  return (
    <HomeStyle>
      <h1 className="title">SoftMarvel</h1>
      <SearchForm onSubmit={handleSubmit} />
      <CharacterCardListStyle>
        {
          loading
            ? <Loader />
            : characters.length === 0
              ? <NoContent />
              : characters.map(({ id, name, thumbnail }) => (
                <CharacterCardStyle key={id} to={`/details/${id}`}>
                  <img className="image" src={thumbnail} alt={name} />
                  <div className="content">
                    <h3  className="name">{name}</h3>
                  </div>
                </CharacterCardStyle>
              ))
        }
      </CharacterCardListStyle>
    </HomeStyle>
  )
};

export default Home;
