import styled, { createGlobalStyle } from 'styled-components';

export const BaseStyle = createGlobalStyle`
  * {
   margin: 0;
   box-sizing: border-box;
   font-family: 'Helvetica', sans-serif;
  }
  img {
    width: 100%;
    max-width: 100%;
  }
  button {
    height: 40px;
    width: auto;
    border: none;
    border: 1px solid #000;
    background-color: #C7C7C7;
    border-radius: 5px;
    font-weight: bold;
  }
  button.button-small {
    font-size: 12px;
    height: 25px;
    line-height: 25px;
  }
`

const AppStyle = styled.div.attrs({ className: 'AppStyle' })`
  width: 90%;
  max-width: 920px;
  margin: 0 auto;
`;

export default AppStyle;
