// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';
import React from 'react';
import { Router } from 'react-router-dom';
import { render, act } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { MockedProvider } from '@apollo/react-testing';


jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useParams: () => ({ id: '1' }),
  useRouteMatch: () => ({ url: '/' }),
}));

global.runAsyncTimers = async () => act(async () => {
  jest.useFakeTimers();
  jest.runAllTimers();
});

global.renderComponentToTest = (Component, { mocks = [], props } = {}) => {
  const history = createMemoryHistory();
  return render(
    <MockedProvider mocks={mocks} addTypename={false}>
      <Router history={history}>
        <Component {...props} />
      </Router>
    </MockedProvider>
  );
};
