import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import AppStyle, { BaseStyle } from './App.style';
import Details from './pages/Details/Details';
import Home from './pages/Home/Home';

const App = () => (
  <AppStyle>
    <BaseStyle />
    <Router>
      <Switch>
        <Route path="/details/:id">
          <Details />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  </AppStyle>
);

export default App;
