import React from "react";
import NoContentStyle from './NoContent.style';

const NoContent = () => (
  <NoContentStyle>
    <h5>Nenhum resultado encontrado</h5>
  </NoContentStyle>
);

export default NoContent;
