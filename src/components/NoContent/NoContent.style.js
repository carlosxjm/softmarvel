import styled from 'styled-components';

const NoContentStyle = styled.div.attrs({ className: 'NoContentStyle' })`
  width: 100%;
  height: 400px;
  background: #C7C7C7;
  color: #7C7C7C;
  font-size: 25px;
  display: flex;
  align-items: center;
  justify-content: center;

`;

export default NoContentStyle;
