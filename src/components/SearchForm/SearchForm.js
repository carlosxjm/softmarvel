import React, { useState } from 'react';
import SearchFormStyle from './SearchForm.style';

const SearchForm = ({ onSubmit }) => {
  const [value, setValue] = useState('');
  return (
    <SearchFormStyle onSubmit={(event) => {
      event.preventDefault();
      onSubmit(value);
    }}>
      <input
        name="search"
        type="search"
        placeholder="Busque um personagem"
        value={value}
        onChange={(event) => setValue(event.target.value)}
      />
      <button type="submit">Buscar</button>
    </SearchFormStyle>
  );
}

export default SearchForm;
