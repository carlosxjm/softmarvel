import styled from 'styled-components';

const SearchFormStyle = styled.form.attrs({ className: 'SearchFormStyle' })`
  position: relative;
  padding: 30px 0;
  max-width: 500px;
  margin: 0 auto;
  input {
    height: 40px;
    width: calc(100% - 60px);
    border: 1px solid;
    border-radius: 5px 0 0 5px;
    padding-left: 10px;
  }

  button {
    border-radius: 0 5px 5px 0;
    border-left: none;
    width: 60px;
  }
`;

export default SearchFormStyle;
