import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import SearchForm from './SearchForm';


describe('SearchForm component', () => {
  it('renders an input', () => {
    const { getByPlaceholderText } = global.renderComponentToTest(SearchForm);
    global.runAsyncTimers();

    const input = getByPlaceholderText('Busque um personagem');
    expect(input).toBeInTheDocument();
  });

  it('triggers submit callback with the right value', () => {
    const onSubmit = jest.fn();
    const { getByPlaceholderText, getByText } = render(<SearchForm onSubmit={onSubmit} />);
    global.runAsyncTimers();

    const inputValue = 'Spider';
    const input = getByPlaceholderText('Busque um personagem');
    const button = getByText('Buscar');

    fireEvent.change(input, { target: { value: inputValue } });
    fireEvent.click(button, { key: 'Enter', code: 'Enter' });
    global.runAsyncTimers();

    expect(onSubmit).toBeCalled();
    expect(onSubmit).toBeCalledWith(inputValue);
  });
});

