import styled from 'styled-components';

const EditFormStyle = styled.form.attrs({ className: 'EditFormStyle' })`
  position: relative;
  .modal {
    display: ${({ isOpen }) => isOpen ? 'flex' : 'none'};
    flex-direction: column;
    position: fixed;
    width: 90%;
    max-width: 400px;
    background-color: #FFF;
    z-index: 11;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    padding: 30px;
    * {
      margin-bottom: 20px;
    }

    textarea {
      min-height: 200px;
      max-height: 50vh;
      padding: 10px;
    }

    .button-close {
      position: absolute;
      top: 0;
      right: 0;
      width: 50px;
      heigth: 50px;
      background: none;
      border: none;
      &:before,
      &:after {
        content: '';
        width: 2px;
        height: 30px;
        background: #000;
        position: absolute;
        top: 0;
        transform: rotate(45deg);
      }
      &:before {
        transform: rotate(-45deg);
      }
    }
  }
  .shadow {
    display: ${({ isOpen }) => isOpen ? 'block' : 'none'};
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: #000;
    opacity: 0.6;
    z-index: 10;
  }
`;

export default EditFormStyle;
