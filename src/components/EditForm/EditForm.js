import React, { useState } from 'react';
import useUpdateCharacter from '../../hooks/useUpdateCharacter';
import EditFormStyle from './EditForm.style';

const EditForm = ({ characterId, onSubmit }) => {
  const [description, setDescription] = useState('');
  const [isOpen, setIsOpen] = useState(false);

  const updateCharacter = useUpdateCharacter();

  return (
    <>
      <EditFormStyle
        isOpen={isOpen}
        onSubmit={(event) => {
          event.preventDefault();
          updateCharacter({ id: characterId, description });
          setIsOpen(false);
          onSubmit();
        }}
      >
        <div className="modal">
          <h4>Edite a descrição do personagem</h4>
          <textarea
            name="description"
            placeholder="Descrição do personagem"
            onChange={(event) => setDescription(event.target.value)}
          />
          <button type="submit">Salvar</button>
          <button type="button" className="button-close" onClick={() => setIsOpen(false)}></button>
        </div>
        <div className="shadow"></div>
        <button
          type="button"
          onClick={() => setIsOpen(true)} className="edit-button button-small"
        >
          Editar Descrição
        </button>
      </EditFormStyle>
    </>
  );
}

export default EditForm;
