import styled from 'styled-components';

const LoaderStyle = styled.svg.attrs({ className: 'LoaderStyle' })`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: trasnlate(-50%, -50%);
`;

export default LoaderStyle;
